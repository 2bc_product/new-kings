 #include "ofApp.h"
#include "Game.h"
// #include <zmq.hpp>
#include <wiringPi.h>

#define RED 0xed1c24
#define BLUE 0x37c6f4

// zmq::context_t context (1);
// zmq::socket_t pub_sock(context, ZMQ_PUB);
// zmq::socket_t sub_sock(context, ZMQ_SUB);

void ofApp::setup(){
  //NETWORKING
  // int linger = 0;
  // pub_sock.setsockopt(ZMQ_LINGER, &linger, sizeof(linger));
  // sub_sock.setsockopt(ZMQ_SUBSCRIBE, "", 0);
  // pub_sock.bind ("tcp://*:5675");
  // sub_sock.bind ("tcp://*:5674");
  // int major, minor, patch;
  // zmq_version(&major, &minor, &patch);
  // ofLog()<< "Current ØMQ version is "+ofToString(major) + ofToString(minor) + ofToString(patch);
  ofSetFrameRate(15);

  ofx::HTTP::BaseServerSettings settings;
  settings.setPort(11111);
  server.setup(settings);
  server.start();
  ofAddListener(server.events.onHTTPServerEvent, this, &ofApp::onServerRequest);

  // load up local settings for torches
  ofxXmlSettings xmlSettings;
  xmlSettings.loadFile("settings.xml");
  torchSettings.one[0] = xmlSettings.getValue("settings:one:r", "40");
  torchSettings.one[1] = xmlSettings.getValue("settings:one:g", "40");
  torchSettings.one[2] = xmlSettings.getValue("settings:one:b", "40");

  torchSettings.two[0] = xmlSettings.getValue("settings:two:r", "40");
  torchSettings.two[1] = xmlSettings.getValue("settings:two:g", "40");
  torchSettings.two[2] = xmlSettings.getValue("settings:two:b", "40");

  torchSettings.ready[0] = xmlSettings.getValue("settings:ready:r", "40");
  torchSettings.ready[1] = xmlSettings.getValue("settings:ready:g", "40");
  torchSettings.ready[2] = xmlSettings.getValue("settings:ready:b", "40");

  torchSettings.waiting[0] = xmlSettings.getValue("settings:waiting:r", "40");
  torchSettings.waiting[1] = xmlSettings.getValue("settings:waiting:g", "40");
  torchSettings.waiting[2] = xmlSettings.getValue("settings:waiting:b", "40");

  torchSettings.intensity = xmlSettings.getValue("settings:intensity", "50");
  torchSettings.blinkRate = xmlSettings.getValue("settings:blinkRate", "500");
  torchSettings.winVibrationDuration = xmlSettings.getValue("settings:winVibrationDuration", "3000");
  
  torchSettings.sensitivity = xmlSettings.getValue("settings:sensitivity","0.8");
    
    
  //return;
  //INITIAL VALUES
  order_teams = 0;
  switch_kings = 0;
  have_kings = 1;
  teams_done = false;
  teams_sent = false;

  for(int i = 0; i<NUM_CONTROLLERS; i++){
    last_tilt[i] = 0;
    last_active_ctrl[i] = 0;
  }

//  ofLog()<<"before gui";
  //SETTINGS GUI
  // gui = new ofxUICanvas();
  // gui->setGlobalCanvasWidth(200);
  ////gui->setFont("Baskerville.dfont");
  //debug_gui.setup();

  float min_king_time = 30000;
  float max_king_time = 70000;
  // gui->addLabel("SETTINGS", OFX_UI_FONT_LARGE);
  // gui->addSpacer();
  // gui->setWidgetFontSize(OFX_UI_FONT_MEDIUM);
  // vector<string> play_names;
  // play_names.push_back("switch king");
  // play_names.push_back("same king");
  // play_names.push_back("no king");
  // gui->addRadio("king_set", play_names, OFX_UI_ORIENTATION_VERTICAL);
  // gui->addLabel("king duration", OFX_UI_FONT_SMALL);
  // gui->addRangeSlider("", 10000, 180000, &min_king_time, &max_king_time);
  // gui->addLabel("team gather duration", OFX_UI_FONT_SMALL);
  // gui->addSlider("", 5000, 30000,  &team_time);
  //
  // gui->addSpacer();
  // vector<string> rad_names;
  // rad_names.push_back("ordered team assignment");
  // rad_names.push_back("random team assignment");
  // gui->addRadio("team_order", rad_names, OFX_UI_ORIENTATION_VERTICAL);
  //
  // gui->addSpacer();
  // vector<string> start_names;
  // start_names.push_back("staggered game start");
  // start_names.push_back("auto game start");
  // gui->addRadio("game_start", start_names, OFX_UI_ORIENTATION_VERTICAL);


  //gui->addSpacer();
  //gui->addLabel("in_msg", OFX_UI_FONT_SMALL);
  //gui->addTextArea("msg", debug_in_msg, OFX_UI_FONT_SMALL);

  // gui->loadSettings("game_settings.xml");
  // gui->autoSizeToFitWidgets();
  // gui->setVisible(false);
  // ofAddListener(gui->newGUIEvent,this,&ofApp::guiEvent);

  //TEAM STUFF
  ofLog()<<"before team allocation";
  teams.resize(2);
  teams[0].id = 0;
  teams[0].color="red";
  teams[0].max_king_ms = max_king_time;
  teams[0].min_king_ms = min_king_time;

  teams[1].id = 1;
  teams[1].color="blue";
  teams[1].max_king_ms = max_king_time;
  teams[1].min_king_ms = min_king_time;
  num_players[0]=0;
  num_players[1]=0;
  team_index = int(ofGetElapsedTimef())%2;

  //FONTS
  ofLog()<<"before font";
  //info.loadFont("coupe-normal.ttf", 62);
  info.loadFont("Sullivan-Fill.otf", 72);
  text.loadFont("coupe-normal.ttf", 45);

  //SOUNDS
  ofLog()<<"before sound";
  join.loadSound("sounds/join.wav");
  idle_music.loadSound("sounds/idle_music.wav");
  team_music.loadSound("sounds/team_music.wav");
  bg_music.loadSound("sounds/bg_music.wav");
  teams[0].new_king.loadSound("sounds/swap.wav");
  teams[1].new_king.loadSound("sounds/swap.wav");
  finish.loadSound("sounds/finish.wav");
  for(int i=0; i<NUM_SCREAMS; i++){
    scream[i].loadSound("sounds/scream_"+ofToString(i)+".wav");
  }
  bg_music.setLoop(true);
  idle_music.setLoop(true);

  //GRAPHICS
  ofLog()<<"before graphics";
  bg_play.loadImage("images/bg_play.jpg");
  bg_idle.loadImage("images/bg_idle.jpg");
  pawn.loadImage("images/pawn.png");
  king.loadImage("images/king.png");

  //pawn.resize(pawn.getWidth()*0.1, pawn.getHeight()*0.1);
  //king_hat.resize(king_hat.getWidth()*0.1, king_hat.getHeight()*0.1);
  bg_play.resize(ofGetWidth(), ofGetHeight());
  bg_idle.resize(ofGetWidth(), ofGetHeight());

  ctrl.resize(NUM_INTRO_FRAMES);
  for(int i=0; i<NUM_INTRO_FRAMES; i++){
    ctrl[i].loadImage("images/animation/main_"+ofToString(i)+".png");
    //ctrl[i].resize(ctrl[i].getWidth()*0.25, ctrl[i].getHeight()*0.25);
  }
  ofLog()<<"animations";

  for(int i=0; i<2; i++){
    for(int j=0; j<5; j++){
      banner[i][j].loadImage("images/banner_"+ofToString(i)+"_"+ofToString(j)+".png");
      banner[i][j].resize(banner[i][j].getWidth()*0.8, banner[i][j].getHeight()*0.8);
    }
  }
  ofLog()<<"banners";
  ofLog()<<"after graphics";
  //    for(int j=0; j<2; j++){
  //      ctrl[i][j].loadImage("images/ctrl_"+ofToString(i)+"_"+ofToString(j)+".png");
  //      ctrl[i][j].resize(ctrl[i][j].getWidth()*0.25,ctrl[i][j].getHeight()*0.25 );
  //    }
  //}
  
  have_kings = 1;
  switch_kings = 0;
  order_teams = 0;
  step_start = 1;
  ofLog()<<"HAVE KING?"+ofToString(have_kings);
  ofLog()<<"ORDER TEAMS?"+ofToString(order_teams);

  // WIRINGPI
  wiringPiSetup();
  pinMode(4, INPUT);
  pullUpDnControl(4, PUD_UP);
  // END WIRINGPI


  transition(WAIT_PLAYER);
}


void ofApp::onServerRequest(ofx::HTTP::ServerEventArgs &evt){
    Poco::URI uri(evt.request().getURI());
    std::string path = uri.getPath(); // just get the path
    std::string stripped = (path[0] == '/' ? path.substr(1) : path); // strip off leading / if it exists
    vector<string> res = ofSplitString(stripped, "/");

    if(res.size() >= 2){

      string action = res[0];
      string pID = res[1];
      // MAIN RESPONSE AREA
      // Calls from Torches should always have at least two params (action, id, [action info])

      // V2 Notes: This is no longer functioning
      if(action == "UI"){
        evt.response().setContentType("text/html");
        std::ostream& outputStream = evt.response().send();

        cout << "LOADING MOBILE UI" << endl;

        Poco::CountingOutputStream ostr(outputStream);
        ostr << "<html>";
        ostr << "<form action='/UI/'>";
        ostr << "<input type='submit' value='Start' style='width=100%; height=100%'/>";
        ostr << "</form>";
        ostr << "</html>";
        ostr.reset();

        //did_transition = true;
        //b_state = true;
      }
      
      // V2 Notes: No Longer in use
      if(res[0] == "BUTTON"){
        //last_b_trans = ofGetElapsedTimeMillis();
        //did_transition = true;
        //b_state = true;
        
        	sendResponse("{status:'OK'}", evt);
      }


      if(action == "STATUS"){
        // update separate collection of devices
        // send settings
        bool isPlayer = false;
        for(vector<Player>::iterator it=active_players.begin(); it!=active_players.end(); ++it){
          if(it->player_id.compare(pID)==0){
            isPlayer = true;
            bool isKing = false;
            if(teams.size() >= 2 && it->team_id != -1){
              if(teams[it->team_id].cur_king){
                if(teams[it->team_id].cur_king->player_id.compare(it->player_id) == 0){
                    isKing = true;
                }
              }
            }
            sendResponse("{status:'" + statusToString() + "',id:'" + it->player_id + "',teamId:'" + ofToString(it->team_id) + "',isKing:'"  + ofToString(isKing) + "', winner:'" + ofToString(winner)  + "'}" , evt);
          }
        }

        if(!isPlayer){
          string settingsPayload = "";
          settingsPayload +=  "oneR:'" + torchSettings.one[0] + "',";
          settingsPayload +=  "oneG:'" + torchSettings.one[1] + "',";
          settingsPayload +=  "oneB:'" + torchSettings.one[2] + "',";

          settingsPayload +=  "twoR:'" + torchSettings.two[0] + "',";
          settingsPayload +=  "twoG:'" + torchSettings.two[1] + "',";
          settingsPayload +=  "twoB:'" + torchSettings.two[2] + "',";

          settingsPayload +=  "rR:'" + torchSettings.ready[0] + "',";
          settingsPayload +=  "rG:'" + torchSettings.ready[1] + "',";
          settingsPayload +=  "rB:'" + torchSettings.ready[2] + "',";

          settingsPayload +=  "wR:'" + torchSettings.waiting[0] + "',";
          settingsPayload +=  "wG:'" + torchSettings.waiting[1] + "',";
          settingsPayload +=  "wB:'" + torchSettings.waiting[2] + "',";

          settingsPayload +=  "intensity:'" + torchSettings.intensity + "',";
          settingsPayload +=  "blink:'" + torchSettings.blinkRate + "',";
          settingsPayload +=  "vibe:'" + torchSettings.winVibrationDuration + "',";
          
          settingsPayload += "sensitivity:'" + torchSettings.sensitivity + "'";

          sendResponse("{status:'"+ statusToString() + "'," + settingsPayload + "}" , evt);
        }

      } else {
        switch (state) {
          case WAIT_PLAYER:
            if(!order_teams){
              if(action == "JOINED"){
                bool new_ctrl = true;
                for(vector<Player>::iterator it=active_players.begin(); it!=active_players.end(); ++it){
                  if(it->player_id.compare(pID)==0)
                    new_ctrl = false;
                }
                if(new_ctrl){
                  Player p = Player(pID);
                  active_players.resize(active_players.size()+1);
                  active_players[active_players.size()-1] = p;
                  ofLog()<<"Player "+ p.player_id +" joined; Num active players:"+ ofToString(active_players.size());
                  join.play();
                  ofLog()<<"active player: "+p.player_id;
                  ofLog()<<"sending flashing active lights";
                  // out_msg->flicker = "1000:500";
                  // out_msg->red = "57";
                  // out_msg->green = "245";
                  // out_msg->blue = "57";
                  // sendMsg(p.player_id, *out_msg);
                  sendResponse("{status:'OK'}",evt);
                } else {
                  sendResponse("{status:'ERROR'}",evt);
                }
              }
            }
          break;
          case PLAY:
            if(action == "TILTED"){
              //DIE LIGHT DIE
              out_msg->red = "0";
              out_msg->green = "0";
              out_msg->blue = "0";
              out_msg->king = "0";
              out_msg->vib = "255";

              sendResponse("{status:'OK'}",evt);
              //string tilt_id=in_msg->id;
              string tilt_id = pID;

              for(vector<Team>::iterator team=teams.begin(); team != teams.end(); ++team){
                map<int,Player>::iterator k=team->players.begin();
                while(k != team->players.end()){
                  if(k->second.player_id.compare(tilt_id) == 0){
                    ofLog()<<"tilted "+ofToString(tilt_id);
                    teams[(team->id+1)%2].score += 100;
                    dead_t dead_p;
                    dead_p.player = k->second;
                    dead_p.time_of_death = ofGetElapsedTimeMillis();
                    dead_players.push_back(dead_p);
                    if((team->players.size()<=1) || (have_kings && team->cur_king->player_id.compare(k->second.player_id) == 0)){
                      //ENTIRE TEAM LOST
                      ofLog()<<"team "+ofToString(team->id)+" LOST";
                      winner = (team->id+1)%2;
                      for(map<int,Player>::iterator l=team->players.begin();l!=team->players.end(); l++){
                        //TURN ALL THE LIGHTS OFF
                        sendMsg(l->second.player_id, *out_msg);
                      }
                      ofLog()<<"about to increment k  "+ ofToString(k->second.player_idx);
                      ++k;
                      transition(WIN);
                    }else{
                      //JUST THE ONE DUDE
                      scream[int(ofRandom(NUM_SCREAMS))].play();
                      ofLog()<<"erasing from team "+team->color+" :"+ofToString(k->second.player_id);
                      sendMsg(k->second.player_id, *out_msg);

                      if(switch_kings && team->next_king != NULL && (team->next_king->player_id.compare(k->second.player_id)==0)){
                        team->players.erase(k++);
                        team->pickNextKing();
                      }else{
                        team->players.erase(k++);
                      }
                      ofLog()<<"players left: "+ofToString(team->players.size());
                    }
                  }
                  else{
                    ++k;
                  }
                }
              }
            }
          break;
          default:
            sendResponse("{status:'ERROR'}",evt);
          break;
        }
      }

    } else {

        if (stripped.empty())
          stripped = "/";

        if(stripped=="/"){
            sendResponse("{status:'ERROR'}", evt);
            return;
        } else{
            evt.response().setStatusAndReason(Poco::Net::HTTPResponse::HTTP_NOT_FOUND);
            return;
        }

    }

}

string ofApp::statusToString(){
  string s;
  switch (state) {
    case WAIT_PLAYER:
      s = "WAIT_PLAYER";
    break;
    case GATHER:
      s = "GATHER";
    break;
    case STARTGAME:
      s = "STARTGAME";
    break;
    case TEAM:
      s = "TEAM";
    break;
    case KINGS:
      s = "KINGS";
    break;
    case PLAY:
      s = "PLAY";
    break;
    case WIN:
      s = "WIN";
    break;
  }
  return s;
}

void ofApp::sendResponse(string res, ofx::HTTP::ServerEventArgs &evt){
  evt.response().setContentType("text/json");
  std::ostream& outputStream = evt.response().send();

  Poco::CountingOutputStream ostr(outputStream);
  ostr << res;
  ostr.reset();
}

void ofApp::readButton(){

  int b_read = !digitalRead(4);
  if (b_read == 1 && last_b_read == 0 && ofGetElapsedTimeMillis() - buttonDelay > buttonTapped){
  
//    last_b_trans = ofGetElapsedTimeMillis();
//    last_b_read = b_read;
//    did_transition = true;
    
    if(state == KINGS){
      buttonTapped = ofGetElapsedTimeMillis();
      transition(PLAY);
    }
    
    if(state == TEAM){
      buttonTapped = ofGetElapsedTimeMillis();
      transition(KINGS);
    }
    
    if(state == WAIT_PLAYER){
      if(!order_teams){
        if(active_players.size()>=2){
          buttonTapped = ofGetElapsedTimeMillis();
          transition(GATHER);
        }
      }
    }
    
    b_state = false;
    did_transition = false;
    
  } else {
     did_transition = false;
  }

  if (ofGetElapsedTimeMillis() - last_b_trans > 50)
     b_state = b_read;
}

void ofApp::update(){

  ////CHECK ALL MESSAGES
  out_msg->red = "0";
  out_msg->green = "255";
  out_msg->blue = "0";
  out_msg->king = "0";
  out_msg->vib = "0";
  out_msg->flicker = "0";


  readButton();
  switch (state) {
    case WAIT_PLAYER:
      if (active_players.size()>=2 && did_transition && b_state)
        transition(GATHER);

      // V2 NOTES: stale code that should never run?
      // else{
      //   if(in_msg->event == "right released" || in_msg->event == "left released"){
      //     bool new_ctrl = true;
      //     for(vector<Player>::iterator it=active_players.begin(); it!=active_players.end(); ++it){
      //       if(it->player_id.compare(in_msg->id) == 0)
      //         new_ctrl = false;
      //     }
      //     if(new_ctrl){
      //       Player p = Player(in_msg->id);
      //       active_players.resize(active_players.size()+1);
      //       active_players[active_players.size()-1] = p;
      //       ofLog()<<"Player "+ p.player_id +" joined; Num active players:"+ ofToString(active_players.size());
      //       int player_index=num_players[team_index]++;
      //       p.team_id=team_index;
      //       p.player_idx=player_index;
      //       teams[team_index].players[player_index] = p;
      //       team_index=(++team_index)%2;
      //       join.play();
      //     }
      //     for(vector<Team>::iterator team=teams.begin(); team != teams.end(); team++){
      //       if(team->color == "red"){
      //         out_msg->red = "240";
      //         out_msg->green = "50";
      //         out_msg->blue = "44";
      //       }else{
      //         out_msg->red = "44";
      //         out_msg->green = "187";
      //         out_msg->blue = "192";
      //       }
      //       for(map<int, Player>::iterator it=team->players.begin(); it!=team->players.end(); it++){
      //         //TURN ON YOUR TEAM COLOR
      //         sendMsg(it->second.player_id, *out_msg);
      //       }
      //     }
      //   }
      // }
      break;
    case GATHER:
      if(!order_teams){
        out_msg->red = "0";
        out_msg->green = "0";
        out_msg->blue = "0";
        out_msg->vib = "0";
        for(int i=0; i<active_players.size(); i++){
          int player_index=num_players[team_index]++;
          active_players[i].team_id = team_index;
          active_players[i].player_idx = player_index;
          teams[team_index].players[player_index] = active_players[i];
          team_index=(++team_index)%2;
          //TURN LIGHT OFF
          	(active_players[i].player_id, *out_msg);
        }
        transition(STARTGAME);
      }
      break;
    case STARTGAME:
      if(!order_teams){
        for(vector<Team>::iterator team=teams.begin(); team != teams.end(); team++){
          string t_players = "TEAM "+ofToString(team->id) +" has ";
          for(map<int, Player>::iterator it=team->players.begin(); it!=team->players.end(); it++){
            t_players += it->second.player_id +"; ";
          }
        }
        out_msg->red = "0";
        out_msg->green = "0";
        out_msg->blue = "0";
        out_msg->vib = "0";
        for(vector<Player>::iterator it=active_players.begin(); it != active_players.end(); it++){
          //TURN LIGHTS OFF
          sendMsg(it->player_id, *out_msg);
        }
        transition(TEAM);
      }
      break;
    case TEAM:
      if(!order_teams){
        if (!step_start && timeInState() > team_time) {
          transition(KINGS);
        }
        if (step_start && did_transition && b_state){
          transition(KINGS);

          b_state = false;
          did_transition = false;
        }
        //FLASH YOUR TEAM COLOR
        if(!teams_sent){
          for(vector<Team>::iterator team=teams.begin(); team != teams.end(); team++){
            out_msg->flicker = "250:100";
            if(team->color == "red"){
              out_msg->red = "240";
              out_msg->green = "50";
              out_msg->blue = "44";
            }else{
              out_msg->red = "44";
              out_msg->green = "187";
              out_msg->blue = "192";
            }
            for(map<int, Player>::iterator it=team->players.begin(); it!=team->players.end(); it++){
              //TURN ON YOUR TEAM COLOR
              ofLog()<<"sending team color information to: "+it->second.player_id;
              ofLog()<<"msg: "+out_msg->toString();
              sendMsg(it->second.player_id, *out_msg);
              ofLog()<<"sent team color info";
            }
          }
          teams_sent = true;
        }

      }
      break;
    case KINGS:
      if (step_start && did_transition && b_state){
        transition(PLAY);
      }
      
      b_state = false;
      did_transition = false;
      
      if(!once){
        for (int i=0; i<2; i++) {
          //teams[i].update(switch_kings, have_kings);
          ofLog() << "UPDATED TEAMS ONCE";
          teams[i].pickNextKing();
          teams[i].cur_king = teams[i].next_king;
          teams[i].cur_king->king_light = true;
          for(map<int, Player>::iterator it=teams[i].players.begin(); it!=teams[i].players.end(); it++){
            if(it->second.king_light){
              out_msg->king = "255";
            }else{
              out_msg->king = "0";
            }
            out_msg->flicker = "250:50";
            if(teams[i].color == "red"){
              out_msg->red = "240";
              out_msg->green = "50";
              out_msg->blue = "44";
            }else{
              out_msg->red = "44";
              out_msg->green = "187";
              out_msg->blue = "192";
            }
            sendMsg(it->second.player_id, *out_msg);
          }
        }
        once = true;
      }
      break;
    case PLAY:
      for (int i=0; i<2; i++) {
        teams[i].update(switch_kings, have_kings);
      }
      out_msg->flicker="0";
      for(vector<Team>::iterator team=teams.begin(); team != teams.end(); team++){
        if(team->color == "red"){
          out_msg->red = "240";
          out_msg->green = "50";
          out_msg->blue = "44";
        }else{
          out_msg->red = "44";
          out_msg->green = "187";
          out_msg->blue = "192";
        }
        for(map<int, Player>::iterator it=team->players.begin(); it!=team->players.end(); it++){
          //TURN YOUR TEAM COLOR
          if(it->second.king_light){
            //ofLog()<<"player "+it->second.player_id +" is a king!";
            out_msg->king = "255";
          }else{
            //ofLog()<<"player "+it->second.player_id +" is not a king";
            out_msg->king = "0";
          }
          sendMsg(it->second.player_id, *out_msg);
        }
      }

      for(vector<dead_t>::iterator dead = dead_players.begin(); dead != dead_players.end(); ++dead){
        if(ofGetElapsedTimeMillis() - dead->time_of_death > 1000){
          out_msg->red = "0";
          out_msg->green = "0";
          out_msg->blue = "0";
          out_msg->king = "0";
          out_msg->vib="0";
          sendMsg(dead->player.player_id, *out_msg);
        }
      }

      break;
    case WIN:
      if (timeInState() > 5000) {
        transition(WAIT_PLAYER);
      }
      out_msg->king = "0";
      out_msg->vib = "250";
      out_msg->flicker = "250:500";
      if(teams[winner].color == "red"){
        out_msg->red = "240";
        out_msg->green = "50";
        out_msg->blue = "44";
      }else{
        out_msg->red = "44";
        out_msg->green = "187";
        out_msg->blue = "192";
      }
      for(map<int,Player>::iterator i=teams[winner].players.begin(); i!=teams[winner].players.end(); i++){
        //FLASH ALL LEDS ON AND OFF FOR THE WIN
        sendMsg(i->second.player_id, *out_msg);
      }
      break;
  }
}

void ofApp::onEnterState() {
  //There's a lot of repeatedly sending osc messages in hopes the controllers get em
  CtrlMsg *off_msg = new CtrlMsg();
  off_msg->red = "0";
  off_msg->green = "0";
  off_msg->blue = "0";
  off_msg->king = "0";
  off_msg->vib = "0";
  off_msg->flicker = "0";

  switch (state) {
    case WAIT_PLAYER:
      for(vector<Player>::iterator it=active_players.begin(); it != active_players.end(); it++){
        //SEND ALL LIGHTS OFF
        sendMsg(it->player_id, *off_msg);
      }
      for (vector<Team>::iterator team=teams.begin(); team!=teams.end(); team++) {
        ofLog()<<"resetting game info";
        team->reset();
        num_players[0]=0;
        num_players[1]=0;
        active_players.clear();
        dead_players.clear();
        for(int i=0; i<NUM_CONTROLLERS; i++){
         last_active_ctrl[i]=0;
        }
      }
      idle_music.play();
      break;
    case STARTGAME:
      for(vector<Team>::iterator team=teams.begin(); team != teams.end(); team++){
        for(map<int, Player>::iterator it=team->players.begin(); it != team->players.end(); it++){
          //SEND ALL LIGHTS OFF
          sendMsg(it->second.player_id, *off_msg);
        }
      }
      break;
    case TEAM:
      teams_sent = false;
      pawn.loadImage("images/pawn.png");
      idle_music.stop();
      team_music.play();
      break;
    case PLAY:
      winner = -1;
      if(have_kings){
        for (vector<Team>::iterator team=teams.begin(); team!=teams.end(); team++) {
          team->pickNextKing();
        }
      }
      team_music.stop();
      bg_music.play();
      break;
    case WIN:
      for(vector<Player>::iterator it=active_players.begin(); it != active_players.end(); it++){
        //SEND ALL LIGHTS OFF
        sendMsg(it->player_id, *off_msg);
      }
      finish.play();
      break;
  }
  delete off_msg;
}

void ofApp::onExitState() {
  ////There's a lot of repeatedly sending osc messages in hopes the controllers get em
  CtrlMsg *off_msg = new CtrlMsg();
  off_msg->red = "0";
  off_msg->green = "0";
  off_msg->blue = "0";
  off_msg->king = "0";
  off_msg->vib = "0";
  off_msg->flicker = "0";

  switch (state) {
    case WAIT_PLAYER:
      //gui->setVisible(false);
      break;
    case STARTGAME:
      for(vector<Team>::iterator team=teams.begin(); team != teams.end(); team++){
        for(map<int, Player>::iterator it=team->players.begin(); it != team->players.end(); it++){
          //SEND ALL LIGHTS OFF
          sendMsg(it->second.player_id, *off_msg);
        }
      }
      break;
    case TEAM:
      team_music.stop();
      teams_sent = false;
      once = false;
      break;
    case PLAY:
      for(vector<Player>::iterator it=active_players.begin(); it != active_players.end(); it++){
        //SEND ALL LIGHTS OFF
          sendMsg(it->player_id, *off_msg);
      }
      bg_music.stop();
    break;
    case WIN:
      teams_sent = false;
      for(vector<Player>::iterator it=active_players.begin(); it != active_players.end(); it++){
        //SEND ALL LIGHTS OFF
        sendMsg(it->player_id, *off_msg);
      }
    break;
  }

  delete off_msg;
}

void ofApp::sendMsg(CtrlMsg &msg){
  string data = msg.toString();
  // zmq::message_t zmq_msg(data.length());
  // memcpy (zmq_msg.data(), data.c_str(), data.length()+1);
  int byte = -1; // OLD ZMQ //pub_sock.send(zmq_msg, 0);
}

void ofApp::sendMsg(string id, CtrlMsg &msg){
  msg.id = id;
  string data = msg.toString();
  // zmq::message_t zmq_msg(data.length()+1);
  // memcpy (zmq_msg.data(), data.c_str(), data.length()+1);
  // OLD ZMQ pub_sock.send(zmq_msg, ZMQ_NOBLOCK);
}

bool ofApp::getMsg(ServMsg &msg){
  //zmq::message_t *raw_msg = new zmq::message_t(sizeof(ServMsg));
  return false;
  // OLD ZMQ
  // if(sub_sock.recv(raw_msg, 256, ZMQ_DONTWAIT)){
  //   string data = static_cast<char*>(raw_msg->data());
  //   msg.parse(data);
  //   delete raw_msg;
  //   return true;
  // }else{
  //   delete raw_msg;
  //   return false;
  // }

}

// void ofApp::guiEvent(ofxUIEventArgs &e){
//   string name = e.widget->getName();
//   int kind = e.widget->getKind();
//
//   if(name =="switch king"){
//     ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
//     switch_kings = toggle->getValue();
//     have_kings = 1;
//   }else if (name == "same king"){
//     ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
//     have_kings = 1;
//     switch_kings = 0;
//   }else if (name == "no king"){
//     ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
//     have_kings = 0;
//   }
//   if(name == "ordered team assignment"){
//     order_teams = 1;
//   }else if(name == "random team assignment"){
//     order_teams=0;
//   }
// }


void ofApp::draw(){
  //bg_idle.draw(0,0);
  //return;
  //if (debug_msg_show){
  //        debug_gui.begin();
  //        ImGui::Text("in msg:");
  //        char * str = (char *)debug_in_msg.c_str();
  //        ImGui::Text(str);
  //        debug_gui.end();
  //}
  ofSetColor(255, 255, 255, 190);
  if(state == WAIT_PLAYER || state == GATHER){
    bg_idle.draw(0,0);
  }else{
    bg_play.draw(0,0);
  }
  ofSetColor(ofColor::white);

  switch (state) {
    case WAIT_PLAYER:
      //INSTRUCTIONS
      ctrl[ofGetElapsedTimeMillis()/125 %NUM_INTRO_FRAMES].draw(ofGetWidth()*0.6, ofGetHeight()*0.27);
      //DRAW pawnS
      if(!order_teams && active_players.size()>0){
        for(int i=0; i<active_players.size(); i++){
          ofFill();
          pawn.draw(ofGetWidth()*0.1+(pawn.getWidth()+10)*(i%10)-pawn.getWidth()/2,
            ofGetHeight()*11/12-pawn.getHeight()-10*sin(ofGetElapsedTimef()));
            //ofGetHeight()*11/12-(200*int(i/10))-pawn.getHeight()-10*sin(ofGetElapsedTimef()));
        }
      }else if (order_teams){
        for(vector<Team>::iterator team=teams.begin(); team != teams.end(); team++){
          for(int i=0; i<team->players.size(); i++){
            ofFill();
            pawn.draw(ofGetWidth()*0.1+(pawn.getWidth()+10)*(i%10)-pawn.getWidth()/2,
              ofGetHeight()*11/12-pawn.getHeight()-10*sin(ofGetElapsedTimef()));
            //pawn.draw(ofGetWidth()*0.34+(pawn.getWidth()+10)*(i%10)-pawn.getWidth()/2, ofGetHeight()*9/12-(-100*team->id)-pawn.getHeight()-10*sin(ofGetElapsedTimef()));
          }
        }
      }

      break;
    case TEAM:{
      //START HERE
      if (!step_start)
        info.drawString(ofToString(int((team_time - timeInState())/1000 + 1 )), ofGetWidth()/2-10, ofGetHeight()/2-10);
      else
        info.drawString(ofToString("READY"), ofGetWidth()/2-35, ofGetHeight()/4-10);

      //text.drawString(ofToString(int((team_time - timeInState())/1000)), ofGetWidth()/2-20, 700);
      for(vector<Team>::iterator team=teams.begin(); team != teams.end(); team++){
        int counter=1+int(timeInState()/(9600/team->players.size()));
        if(team->id) ofSetHexColor(BLUE);
        else ofSetHexColor(RED);
        for(int i=0; i<counter; i++){
          if (i+1 <= team->players.size()){
            if(!team->id)
              pawn.draw((215+(10+pawn.getWidth())*(i%5)-pawn.getWidth()), 280);
            else
              pawn.draw(ofGetWidth()-(215+(10+pawn.getWidth())*(i%5)-pawn.getWidth()), 280);
          }
        }
      }
      }break;
    case KINGS:{
        info.drawString(ofToString("SET"), ofGetWidth()/2-15, ofGetHeight()/4-10);
      //text.drawString(ofToString(int((team_time - timeInState())/1000)), ofGetWidth()/2-20, 700);
      for(vector<Team>::iterator team=teams.begin(); team != teams.end(); team++){
        //int counter=1+int(timeInState()/(9600/team->players.size()));
        if(team->id) ofSetHexColor(BLUE);
        else ofSetHexColor(RED);
        for(int i=0; i<team->players.size(); i++){
          if(!team->id)
            pawn.draw((215+(10+pawn.getWidth())*(i%5)-pawn.getWidth()), 280);
          else
            pawn.draw(ofGetWidth()-(215+(10+pawn.getWidth())*(i%5)-pawn.getWidth()), 280);
        }
      }
      }break;
    case PLAY:
        info.drawString(ofToString("GO!"), ofGetWidth()/2-15, ofGetHeight()/4-10);
      for(vector<Team>::iterator team=teams.begin(); team != teams.end(); team++){
        if(team->id) ofSetHexColor(BLUE);
        else ofSetHexColor(RED);
        for(int i=0; i<team->players.size(); i++){
          if(i == 0){
            if(have_kings){
              if(!team->id)
                king.draw((215), 255);
              else
                king.draw(ofGetWidth()-(215), 255);
            }
          }else{
            if(!team->id)
              pawn.draw((215+(10+pawn.getWidth())*(i%5)), 280);
            else
              pawn.draw(ofGetWidth()-(215+(10+pawn.getWidth())*(i%5)), 280);
          }
        }
      }
      break;
    case WIN:
      if(timeInState() < 1500){
        banner[winner][0].draw(ofGetWidth()/2-(banner[0][2].getWidth()/2+banner[0][0].getWidth()+banner[0][1].getWidth()+20),(timeInState()-1400));
        banner[winner][4].draw(ofGetWidth()/2+(banner[0][2].getWidth()/2+banner[0][1].getWidth()+20),(timeInState()-1400));

        banner[winner][1].draw(ofGetWidth()/2-(banner[0][2].getWidth()/2+banner[0][1].getWidth()+20),(timeInState()-1400));
        banner[winner][3].draw(ofGetWidth()/2+(banner[0][2].getWidth()/2+20),(timeInState()-1400));

        banner[winner][2].draw(ofGetWidth()/2-banner[0][2].getWidth()/2,(timeInState()-1400));
      }else{
        banner[winner][0].draw(ofGetWidth()/2-(banner[0][2].getWidth()/2+banner[0][0].getWidth()+banner[0][1].getWidth()+20),(100));
        banner[winner][4].draw(ofGetWidth()/2+(banner[0][2].getWidth()/2+banner[0][1].getWidth()+20),(100));

        banner[winner][1].draw(ofGetWidth()/2-(banner[0][2].getWidth()/2+banner[0][1].getWidth()+20),(100));
        banner[winner][3].draw(ofGetWidth()/2+(banner[0][2].getWidth()/2+20),(100));

        banner[winner][2].draw(ofGetWidth()/2-banner[0][2].getWidth()/2,(100));
      }
      break;
  }
  if (debug_msg_show){
    text.drawString(debug_in_msg, 50, 100);
  }
}

void ofApp::exit(){
  ofRemoveListener(server.events.onHTTPServerEvent, this, &ofApp::onServerRequest);
  //gui->saveSettings("game_settings.xml");
  delete in_msg;
  delete out_msg;
  //delete gui;

}
void ofApp::keyPressed(int key){
  //GAME START
  if(state == WAIT_PLAYER){
    if(key == 's'){
      //gui->toggleVisible();
      cout << "formerly this would open gui" << endl;
    }
    if(key == 'm'){
      debug_msg_show = ! debug_msg_show;
    }
    if(key && !order_teams){
      if(active_players.size()>=2)
        transition(GATHER);
    }else if (key && order_teams){
      if(teams[0].players.size()>0 && teams[1].players.size() >0)
        transition(PLAY);
    }
  }
  if(key && step_start && state == TEAM)
    transition(KINGS);

  if(key && step_start && state == KINGS)
    transition(PLAY);

}

void ofApp::keyReleased(int key){

}

void ofApp::mouseMoved(int x, int y){

}

void ofApp::mouseDragged(int x, int y, int button){

}

void ofApp::mousePressed(int x, int y, int button){

}

void ofApp::mouseReleased(int x, int y, int button){

}

void ofApp::windowResized(int w, int h){

}

void ofApp::gotMessage(ofMessage msg){

}

void ofApp::dragEvent(ofDragInfo dragInfo){

}

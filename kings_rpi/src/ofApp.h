#pragma once

#include "ofMain.h"
//#include "ofxRemoteUIServer.h"
//#include "ofxUI.h"
#include "ofxTimer.h"
//#include "ofxZmq.h"
#include "Game.h"
//#include "ofxOsc.h"
#include "ofxHTTP.h"
#include "SimpleServer.h"
#include "Poco/CountingStream.h"
#include "ofxXmlSettings.h"

//#include "ofxImGui.h"

#define NUM_MSG_STRINGS 20
#define NUM_CONTROLLERS 20
#define NUM_SCREAMS 2
#define NUM_INTRO_FRAMES 107
#define PUB_PORT 5674
#define SUB_PORT 5675

#define STATES C(START)C(WAIT_PLAYER)C(GATHER)C(STARTGAME)C(TEAM)C(KINGS)C(PLAY)C(WIN)
#define C(x) x,
typedef enum { STATES NUM_STATES } state_t;
#undef C
#define C(x) #x,
const char* const state_names[] = { STATES };


class ofApp : public ofBaseApp{
  public:
    void setup();
    void update();
    void draw();
    void exit();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
    void readButton();
    void onServerRequest(ofx::HTTP::ServerEventArgs &evt);

    void sendResponse(string res, ofx::HTTP::ServerEventArgs &evt);
    string statusToString();

    ofx::HTTP::SimpleServer server;

    //ZMQ
    //ofxOscSender sender;
    //ofxOscReceiver receiver;

    ServMsg *in_msg = new ServMsg();
    CtrlMsg *out_msg = new CtrlMsg();
    void sendMsg(CtrlMsg &msg);
    void sendMsg(string id, CtrlMsg &msg);
    bool getMsg(ServMsg &msg);

    string debug_in_msg = "nothing yet";
    //ofxImGui debug_gui;
    bool debug_msg_show = false;

    //SETTINGS
    //ofxUICanvas *gui;
    //void guiEvent(ofxUIEventArgs &e);
    bool order_teams=0;
    bool switch_kings=0;
    bool have_kings=1;
    bool step_start=1;

    TorchSettings torchSettings;

    //BUTTON THANGS
    int b_state, last_b_read;
    long last_b_trans;
    bool once = false;
    bool did_transition = false;

    int num_players[2];
    int last_active_ctrl[NUM_CONTROLLERS];
    int last_tilt[NUM_CONTROLLERS];
    bool teams_done;

    float timers[NUM_MSG_STRINGS];

    ////SOUNDS
    ofSoundPlayer bg_music;
    ofSoundPlayer finish;
    ofSoundPlayer join;
    ofSoundPlayer idle_music;
    ofSoundPlayer team_music;
    ofSoundPlayer scream[NUM_SCREAMS];

    ////GRAPHICS
    ofImage banner[2][5];
    //ofImage control_animation[104];
    //ofImage ctrl[2][2];
    vector<ofImage> ctrl;
    ofImage bg_idle;
    ofImage bg_play;
    ofImage pawn;
    ofImage king;

    vector<Player> active_players;
    vector<dead_t> dead_players;
    vector<Team> teams;

    state_t state;
    long state_start_at;
    string cur_state_name;

    int gather_ms = 5000;
    float team_time=9600;
    int winner = -1;
    int flicker_ms = 2000;
    bool teams_sent=false;
  
    int buttonDelay = 1000;
    long buttonTapped = 0;

    int team_index;
    ofTrueTypeFont header_text;
    ofTrueTypeFont info;
    ofTrueTypeFont text;

    string start_instr;

    void transition(state_t _state) {
      onExitState();
      ofLog() << "transition from " << state_names[state] << " to " << state_names[_state];
      state_start_at = ofGetElapsedTimeMillis();
      state = _state;
      cur_state_name = state_names[state];
      onEnterState();
    }

    long timeInState() {
      return ofGetElapsedTimeMillis() - state_start_at;
    }

    void onEnterState();
    void onExitState();


};

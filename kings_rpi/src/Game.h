#pragma once


class CtrlMsg{
  public:
    string id;
    string flicker;
    string red;
    string green;
    string blue;
    string king;
    string vib;

    string toString(){
      string s = "";
      s += id +"/";
      s += red +"/";
      s += green +"/";
      s += blue +"/";
      s += king +"/";
      s += vib +"/";
      s += flicker +"/";
      return s;
    }
};

class ServMsg{
  public:
    string id;
    string event;

    void parse(string data){
      vector<string> res = ofSplitString(data, "/");
      id = res[0];
      event = res[1];
    }

    string toString(){
      string s = id+"/"+event;
      return s;
    }
};


class Player {
public:
  int team_id;
  int player_idx;
  string player_id;
  string name;
  bool king_light;
  bool active;

  ofxTimer hit_timer;

  Player() {}
  Player( string _player_id, int _idx=0, int _team_id=-1, string _name="?") {
    player_idx=_idx;
    team_id = _team_id;
    player_id = _player_id;
    name = _name;
    active = true;
  }

  void hit() {
    ofLog() << "("<<team_id<<":"<<player_id<<") hit";
    hit_timer.start();
    active = false;
  }

  void draw() {
    int x = ofGetWidth()/20 + player_idx*ofGetWidth()/10;
    int y = ofGetHeight()/3 + team_id*ofGetHeight()/3;
    int r = ofGetWidth()/20;
    if (hit_timer.active() && hit_timer.age() < 100) {
      ofSetColor(ofColor::green);
    } else {
      ofSetColor(team_id == 0 ? ofColor::red : ofColor::blue);
    }
    ofCircle(x,y,r);
    if (king_light) {
      ofSetColor(ofColor::gold);
      ofCircle(x,y,r/2);
    }
    ofSetColor(255);
    ofDrawBitmapString(name, x, y);
  }

};
typedef struct dead{
  Player player;
  long time_of_death;
}dead_t;

class Team {
public:
  int id;
  int score;
  string color;
  map<int,Player> players;
  Player *cur_king;
  Player *next_king;
  ofxTimer king_timer;
  ofSoundPlayer new_king;
  float flicker_ms;
  float min_king_ms;
  float max_king_ms;

  Team(){
    flicker_ms = 2000;
  }

  void reset() {
    players.clear();
    score=0;
    cur_king = next_king = NULL;
  }
  void update(bool switch_kings, bool have_kings) {
    for (map<int,Player>::iterator it=players.begin(); it!=players.end(); it++) {
      it->second.king_light = false;
    }
    if(have_kings){
      if (cur_king != NULL) {
        cur_king->king_light = true;
        if(switch_kings){
          if (king_timer.active() && king_timer.remaining() < flicker_ms) {
            if (next_king == NULL){
              ofLog()<<"picking king";
              pickNextKing();
            }
            // flicker
            cur_king->king_light = ofGetElapsedTimeMillis() % 250 < 125;
            next_king->king_light = ofGetElapsedTimeMillis() % 250 > 125;
          } else if (king_timer.expired()) {
            ofLog() << "team " << id << " king timer expired";
            cur_king = NULL;
            king_timer.clear();
          }
        }
      } else {
        cur_king = next_king;
        next_king = NULL;
        ofLog() << "team " << id << " crowned " << cur_king->player_id << " as current king";
        king_timer.set_duration(ofRandom(min_king_ms, max_king_ms));
        king_timer.start();
      }
    }
  }

  void draw() {
    for (map<int,Player>::iterator it=players.begin(); it!=players.end(); it++) {
      it->second.draw();
    }
  }

  bool hit(string player_id) {
    for (map<int,Player>::iterator it=players.begin(); it!=players.end(); it++) {
      if(it->second.player_id.compare(player_id)==0){
        it->second.hit();
      }
    }
    //players[player_id].hit();
    if (!cur_king) return false;
    if (cur_king->player_id.compare(player_id) == 0) {
      return true;
    } else {
      return false;
    }
  }

  void pickNextKing() {
    do {
      map<int,Player>::iterator rand=players.begin();
      std::advance(rand, ofRandom(0, players.size()));
      next_king = &players[rand->first];
    } while (players.size()>1 && (next_king == cur_king));
    ofLog() << "team " << id << " selected " << next_king->player_id << " as next king";
    new_king.play();
  }
};


class TorchSettings {
public:
  string one[3];
  string two[3];
  string waiting[3];
  string ready[3];
  string intensity;
  string blinkRate;
  string winVibrationDuration;
  
  string sensitivity;
  
  TorchSettings(){

  }
};

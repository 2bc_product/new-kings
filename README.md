# Kings - V2

Rebuild of Kings, Jeff Gray - April-May, 2018

## Overview

#### Project Layout

Project files are organized into Server code (kings_rpi), firmware (kings_firmware), and Eagle hardware pcb design files (kings_pcb).

The **misc** folder contains additional documentation as a hot-sheet for on-site maintenance or settings tweaks.

#### Server

Server has been refactored from V1 using standard HTTP communication, built in openFrameworks. Compiling the server can be done using msys2 (Windows), XCode (OSX), or with make files (Raspberry Pi). As of May 19, 2018, the server is built using the latest openFrameworks (0.10 stable).

#### Server API

All calls are made from Client (torches) to Server. The server exposes three primary calls (STATUS, JOINED, TILTED), using simple GET requests, with a response delivered in JSON. The ID parameter is generated using the fourth octet of the torch's DHCP-assigned local IP address, ensuring unique IDs in each session.

``/STATUS/[ID]``

Params: [ID] (Optional)

 The status command is sent every 700ms from the firmware. If it contains an ID, it will receive ID-related status content (current game mode, player's team, if the player is a King, etc). If an ID is not present, the server will respond with global settings for the torches (torch team colors, brightness, sensor sensitivity, etc).

``/JOINED/[ID]``

 Params: [ID] (Required)

The joined command is only heard successfully during the WAIT_PLAYER mode on the server (the general attract-mode for Kings). This will add the torch that sent it to the roster of the active game.

``/TILTED/[ID]``

Params: [ID] (Required)

The tilted command is only heard successfully during the PLAY mode on the server (the active game mode). This tells the server that this player has died and should be removed from the active roster. In the scenario where the player was a King, this will cause a global game mode change to WIN, prompting other torches to respond accordingly.


#### Hardware

The primary hardware in each torch:
- [Adafruit Huzzah ESP8266](https://www.adafruit.com/product/2821)
- [Adafruit BNO055 IMU](https://www.adafruit.com/product/2472)
- [Adafruit NeoPixel 5mm Through-Hole LEDs](https://www.adafruit.com/product/1938)
- [Adafruit Vibrating Motor](https://www.adafruit.com/product/1201)

#### Firmware

The firmware is written in Arduino, and uses the following libraries:
- [ESP8266Wifi Stack](https://github.com/esp8266/Arduino) - (included with ESP8266 board installation)
- [Bounce2](https://github.com/thomasfredericks/Bounce2) - Button debouncing
- [Wire](https://www.arduino.cc/en/Reference/Wire) - i2C communication (for BNO055)
- [Adafruit Sensor](https://github.com/adafruit/Adafruit_Sensor) - General purpose Adafruit sensor library
- [Adafruit BNO055](https://github.com/adafruit/Adafruit_BNO055) - 9dof Absolute Position IMU library
- [FastLED](https://github.com/FastLED/FastLED) - NeoPixel manipulation
- [ArduinoJson](https://github.com/bblanchon/ArduinoJson) - JSON Parsing
- [ESPAsyncTCP](https://github.com/me-no-dev/ESPAsyncTCP) - Asynchronous TCP library (for non-blocking comm with server)

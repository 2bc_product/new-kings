#include <AsyncPrinter.h>
#include <async_config.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncTCPbuffer.h>
#include <tcp_axtls.h>

// Networking Includes
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>

// Additional Includes
#include <Bounce2.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#include "FastLED.h"
#include "ArduinoJson.h"

// Pin Defines
#define LEFTVIBEPIN 12
#define RIGHTVIBEPIN 13
#define BUTTONPIN 14
#define NEOPIN 2
#define LEDPIN LED_BUILTIN 

// Init Defines
Bounce button = Bounce();

// Neopixel
#define COLOR_ORDER RGB                                       
#define LED_TYPE WS2812                                       
#define NUM_LEDS 8 
uint8_t max_bright = 75;  
struct CRGB leds[NUM_LEDS];  

// Network
ESP8266WiFiMulti WiFiMulti;
char ssid[] = "Kings AP";
char pass[] = "ch3ckmat3";
char server[] = "192.168.1.111";
int port = 11111;
HTTPClient httpClient;
int POLLING_RATE = 700;

static AsyncClient * pollingClient = NULL;
static AsyncClient * actionClient = NULL;

// IMU
#define BNO055_SAMPLERATE_DELAY_MS (100)
imu::Quaternion cQ;
Adafruit_BNO055 bno = Adafruit_BNO055(55);

// Globals
long sinceButtonStart;
long sinceLastPoll;
long sinceLastTiltCheck;
bool joined = false;

int teamOneColor[3] = { 255, 0, 0 };
int teamTwoColor[3] = { 0, 255, 0 };
int waitingColor[3] = { 0, 0, 0 };
int readyColor[3] = { 255, 255, 255 };

float sensitivity = 0.8;

int blinkRate = 200;
int winVibeRate = 1500;
String gameStatus = "WAIT_PLAYER";

bool isPlaying = false;
bool isTilted = false;
bool isWinner = false;
long winCycle;
bool isTeamOne = false;
bool isKing = false;
bool enteredWinState = false;

void setup() {
  initPins();
  initNeopixels();
  setColor(false, readyColor[0], readyColor[1], readyColor[2]);
  initComms();
  initIMU();

  displaySensorDetails();
  vibeStartup(1);
}



void loop() {
  if(WiFi.status() == WL_CONNECTED) {
    digitalWrite(LEDPIN, HIGH);
    
    // Poll at Polling Rate Interval
    if(millis() - sinceLastPoll > POLLING_RATE){
      sinceLastPoll = millis();
      getAsync(String("STATUS"), String(WiFi.localIP()[3]));
    }

    if(millis() - sinceLastTiltCheck > POLLING_RATE * 2){
      sinceLastTiltCheck = millis();
      if(isTilted && gameStatus.equals("PLAY")){
        getActionAsync(String("TILTED"), String(WiFi.localIP()[3]));
        Serial.println("Tilt Check");
      }
    }

    // Events for each Loop
    button.update();
    
    if(gameStatus.equals("WAIT_PLAYER")){
      isTilted = false;

      // Button Listener
      if(button.fell()){
        sinceButtonStart = millis(); 
      }
  
      if(button.rose()){
        long elapsed = millis() - sinceButtonStart;
        bool longPress = (elapsed > 1000 ? true : false); 
  
        if(longPress){
          Serial.println("Tilted");
          getActionAsync(String("TILTED"), String(WiFi.localIP()[3]));
          isTilted = true;
        } else {
          Serial.println("Joined");
          getActionAsync(String("JOINED"), String(WiFi.localIP()[3])); 
          isPlaying = true;
        }
      }

      if(isPlaying){
        setColor(false, readyColor[0], readyColor[1], readyColor[2]);
      } else {
        setColor(false, waitingColor[0], waitingColor[1], waitingColor[2]);
      }
    } // end of if gamestatus is waiting

    if(isPlaying){
      if(gameStatus.equals("TEAM")){
        
        if(millis() % (blinkRate * 2) > blinkRate){
          setColor(false, (isTeamOne? teamOneColor[0]:teamTwoColor[0]), (isTeamOne? teamOneColor[1]:teamTwoColor[1]), (isTeamOne? teamOneColor[2]:teamTwoColor[2]));
        } else {
          setColor(false, waitingColor[0], waitingColor[1], waitingColor[2]);
        }
      }
  
      if(gameStatus.equals("KINGS")){
        if(millis() % blinkRate > (blinkRate / 2) ){
          setColor(false, (isTeamOne? teamOneColor[0]:teamTwoColor[0]), (isTeamOne? teamOneColor[1]:teamTwoColor[1]), (isTeamOne? teamOneColor[2]:teamTwoColor[2]));
          if(isKing) setColor(true, (isTeamOne? teamOneColor[0]:teamTwoColor[0]), (isTeamOne? teamOneColor[1]:teamTwoColor[1]), (isTeamOne? teamOneColor[2]:teamTwoColor[2]));
        } else {
          setColor(false, waitingColor[0], waitingColor[1], waitingColor[2]);
          if(isKing) setColor(true, waitingColor[0], waitingColor[1], waitingColor[2]);
        }
      }
      
      if(gameStatus.equals("PLAY") && !isTilted){
        // Set Lights
        setColor(false, (isTeamOne? teamOneColor[0]:teamTwoColor[0]), (isTeamOne? teamOneColor[1]:teamTwoColor[1]), (isTeamOne? teamOneColor[2]:teamTwoColor[2]));
        if(isKing) setColor(true, (isTeamOne? teamOneColor[0]:teamTwoColor[0]), (isTeamOne? teamOneColor[1]:teamTwoColor[1]), (isTeamOne? teamOneColor[2]:teamTwoColor[2]));
  
        // Check for Tilt
        bool didTilt = checkIMU();
        if(didTilt){
          Serial.println("Tilted");
          getActionAsync(String("TILTED"), String(WiFi.localIP()[3]));
          isTilted = true;
          resetGame();
        }
      }
  
      if(gameStatus.equals("WIN")){
        if(!enteredWinState){
          // do this once
          enteredWinState = true;
          
          if(isWinner){
            winCycle = millis();
          } else {
            resetGame();
          }
        }
  
        if(isWinner){
          if(millis() - winCycle < winVibeRate){
            if(millis() % 400 > 200){
              digitalWrite(LEFTVIBEPIN, HIGH);
              digitalWrite(RIGHTVIBEPIN, LOW);
            } else {
              digitalWrite(LEFTVIBEPIN, LOW);
              digitalWrite(RIGHTVIBEPIN, HIGH);
            }
          } else {
            resetGame();
          }
        }
      }
    }
    
  } // end of if wifi connected 
}

void resetGame(){
  isPlaying = false;
  //isTilted = false;
  isWinner = false;
  isTeamOne = false;
  isKing = false;
  winCycle = millis();
  enteredWinState = false;
  gameStatus = "";  
  
  setColor(false, waitingColor[0], waitingColor[1], waitingColor[2]);
  delay(10);
  setColor(true, waitingColor[0], waitingColor[1], waitingColor[2]);

  digitalWrite(LEFTVIBEPIN, LOW);
  digitalWrite(RIGHTVIBEPIN, LOW);
}



void setColor(bool king, int r, int g, int b){
  if(!king){
    fill_solid(leds, 4, CRGB( r, g, b));
  } else {
    fill_solid(&(leds[4]), 4, CRGB( r, g, b));
  }
  FastLED.show();
}





void vibeStartup(int count) {
  for(int i=0;i<count;i++){
    digitalWrite(LEFTVIBEPIN, HIGH);
    digitalWrite(RIGHTVIBEPIN, LOW);
    delay(200);
    digitalWrite(LEFTVIBEPIN, LOW);
    digitalWrite(RIGHTVIBEPIN, HIGH);
    delay(200);
    digitalWrite(LEFTVIBEPIN, HIGH);
    digitalWrite(RIGHTVIBEPIN, LOW);
    delay(200);  
    digitalWrite(LEFTVIBEPIN, LOW);
    digitalWrite(RIGHTVIBEPIN, LOW);
  }
}


void displaySensorDetails(void)
{
  sensor_t sensor;
  bno.getSensor(&sensor);
  Serial.println("------------------------------------");
  Serial.print  ("Sensor:       "); Serial.println(sensor.name);
  Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
  Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
  Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println(" xxx");
  Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println(" xxx");
  Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println(" xxx");
  Serial.println("------------------------------------");
  Serial.println("");
  delay(500);
}


void initPins(){
  pinMode(LEDPIN, OUTPUT);
  pinMode(LEFTVIBEPIN, OUTPUT);
  pinMode(RIGHTVIBEPIN, OUTPUT);
  pinMode(BUTTONPIN, INPUT);

  digitalWrite(LEDPIN, LOW);

  button.attach(BUTTONPIN);
  button.interval(5); 
}

void initComms(){
  Serial.begin(115200);
  Serial.flush();
  Serial.println();
  Serial.println("Starting Up");

  WiFi.mode(WIFI_STA);
  //WiFiMulti.addAP(ssid, pass);
  WiFi.begin(ssid, pass);
}

void initNeopixels(){
  LEDS.addLeds<LED_TYPE, NEOPIN, COLOR_ORDER>(leds, NUM_LEDS);  // Use this for WS2812
  FastLED.setBrightness(max_bright);
}

void initIMU(){
  if (!bno.begin()) {
    Serial.println("No BNO055 detected ... Check your wiring or I2C ADDR!");
  } else {
    Serial.println("BNO055 Initialized");
    // bno.setAxisRemap(bno.REMAP_CONFIG_P1); 
    // bno.setAxisSign(bno.REMAP_SIGN_P1);
  }
}
bool checkIMU() {
  /* Get a new sensor event */
  sensors_event_t event;
  bno.getEvent(&event);

  // imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);
  // Quaternion data
  imu::Quaternion q = bno.getQuat();
  imu::Quaternion quat(q.w(), q.x(), q.y(), q.z());
  quat.normalize();

  imu::Vector<3> atRest;
  atRest.x() = 5.0;

  imu::Vector<3> rotatedVec = quat.rotateVector(atRest);

  // normalized value
  float normUp = (5 - sqrt( (rotatedVec.x() * rotatedVec.x()) + (rotatedVec.y() * rotatedVec.y()))) / 5;

  if (normUp > sensitivity) {

    Serial.print(rotatedVec.x()); Serial.print(", ");
    Serial.print(rotatedVec.y()); Serial.print(", ");
    Serial.print(rotatedVec.z()); Serial.print(", ");
    Serial.print( normUp );
    Serial.println();

    //imuString = String(normUp);

    return false;
    digitalWrite(LED_BUILTIN, HIGH);

  } else {
    return true;
    digitalWrite(LED_BUILTIN, LOW);
  }

  return true;
}

void getAsync(String t, String id){
  // Serial.println("Poll");
  if(pollingClient)
    return;

  pollingClient = new AsyncClient();
  if(!pollingClient)//could not allocate client
    return;

  pollingClient->onError([](void * arg, AsyncClient * client, int error){
    pollingClient = NULL;
    delete client;
  }, NULL);

  pollingClient->onConnect([t, id](void * arg, AsyncClient * client){
    pollingClient->onError(NULL, NULL);
  
    client->onDisconnect([](void * arg, AsyncClient * c){
      pollingClient = NULL;
      delete c;
    }, NULL);

    client->onData([t, id](void * arg, AsyncClient * c, void * data, size_t len){
    //  Serial.print("\r\nData: ");
    //  Serial.println(len);
      uint8_t * d = (uint8_t*)data;
      String incoming = String("");

      bool beforeHeaders = true;
      for(size_t i=4; i<len;i++){
        if(!beforeHeaders) incoming += char(d[i]);
        if(d[i-3] == 13 && d[i-2] == 10 && d[i-1] == 13 && d[i] == 10){
          beforeHeaders = false;
        }
      }

      parseResponse(incoming);
      
    }, NULL);

    //send the request
    client->write(String("GET /" + t + "/" + id + " HTTP/1.0\r\n\r\n").c_str());
  }, NULL);

  if(!pollingClient->connect(server, port)){
    Serial.println("Polling Connect Fail");
    AsyncClient * client = pollingClient;
    pollingClient = NULL;
    delete client;
  }
}


void getActionAsync(String t, String id){
  Serial.print("Action: ");
  Serial.println(t);
  if(actionClient)
    return;

  actionClient = new AsyncClient();
  if(!actionClient)//could not allocate client
    return;

  actionClient->onError([](void * arg, AsyncClient * client, int error){
    actionClient = NULL;
    delete client;
  }, NULL);

  actionClient->onConnect([t, id](void * arg, AsyncClient * client){
    actionClient->onError(NULL, NULL);
  
    client->onDisconnect([](void * arg, AsyncClient * c){
      actionClient = NULL;
      delete c;
    }, NULL);

    client->onData([t, id](void * arg, AsyncClient * c, void * data, size_t len){
    //  Serial.print("\r\nData: ");
    //  Serial.println(len);
      uint8_t * d = (uint8_t*)data;
      String incoming = String("");

      bool beforeHeaders = true;
      for(size_t i=4; i<len;i++){
        if(!beforeHeaders) incoming += char(d[i]);
        if(d[i-3] == 13 && d[i-2] == 10 && d[i-1] == 13 && d[i] == 10){
          beforeHeaders = false;
        }
      }

      parseResponse(incoming);
      
    }, NULL);

    //send the request
    client->write(String("GET /" + t + "/" + id + " HTTP/1.0\r\n\r\n").c_str());
  }, NULL);

  if(!actionClient->connect(server, port)){
    Serial.println("Connect Fail");
    AsyncClient * client = actionClient;
    actionClient = NULL;
    delete client;
  }
}

void parseResponse(String incoming){

  DynamicJsonBuffer jsonBuffer;
  
//  int teamOneColor[3];
//  int teamTwoColor[3];
//  int waitingColor[3];
//  int readyColor[3];
//  
//  int blinkRate;
//  int winVibeRate;

  JsonObject& rootJSON = jsonBuffer.parseObject(incoming);
  String currentGameStatus = rootJSON["status"].as<String>();

  // waiting without ID updates
  String intensity = rootJSON["intensity"].as<String>();
  if(!intensity.equals("")){
    gameStatus = currentGameStatus;
    Serial.println(incoming);
    
    teamOneColor[0] = rootJSON["oneR"].as<String>().toInt();
    teamOneColor[0] = rootJSON["oneG"].as<String>().toInt();
    teamOneColor[0] = rootJSON["oneB"].as<String>().toInt();
    
    teamTwoColor[0] = rootJSON["twoR"].as<String>().toInt();
    teamTwoColor[1] = rootJSON["twoG"].as<String>().toInt();
    teamTwoColor[2] = rootJSON["twoB"].as<String>().toInt();

    readyColor[0] = rootJSON["rR"].as<String>().toInt();
    readyColor[1] = rootJSON["rG"].as<String>().toInt();
    readyColor[2] = rootJSON["rB"].as<String>().toInt();

    waitingColor[0] = rootJSON["wR"].as<String>().toInt();
    waitingColor[1] = rootJSON["wG"].as<String>().toInt();
    waitingColor[2] = rootJSON["wB"].as<String>().toInt();

    max_bright = rootJSON["intensity"].as<String>().toInt();
    sensitivity = rootJSON["sensitivity"].as<String>().toFloat();
    
    FastLED.setBrightness(max_bright);

    blinkRate = rootJSON["blink"].as<String>().toInt();
    winVibeRate = rootJSON["vibe"].as<String>().toInt();
  }

  // all other messages come packaged with ID-specific information
  String playerID = rootJSON["id"].as<String>();
  if(!playerID.equals("")){
    Serial.println(incoming);
    gameStatus = currentGameStatus;
    
    if(currentGameStatus.equals("TEAM")){
      isTeamOne = (rootJSON["teamId"].as<String>().equals("0"));
    }

    if(currentGameStatus.equals("KINGS")){
      isKing = (rootJSON["isKing"].as<String>().equals("1"));
    }

    if(currentGameStatus.equals("WIN")){
      isWinner = (isTeamOne && rootJSON["winner"].as<String>().equals("0")) || (!isTeamOne && rootJSON["winner"].as<String>().equals("1"));
    }
  }
  
  Serial.println(currentGameStatus);
  jsonBuffer.clear();
}



void sendBlockingGet(){
  long elapsed = millis() - sinceButtonStart;
  bool longPress = (elapsed > 1000 ? true : false);

  Serial.print("[HTTP] begin...");
  Serial.println(millis());
  String address = String(server);
  if(longPress){
    address += "TILTED";
    joined = false;
  } else {
    if(joined){
      address += "STATUS";
    } else {
      address += "JOINED";
      joined = true;
    }
  }
  address += "/" + String(WiFi.localIP()[3]);
  httpClient.begin(address); 

  Serial.print("[HTTP] GET... " + address + "\n");
  // start connection and send HTTP header
  int httpCode = httpClient.GET();

  // httpCode will be negative on error
  if(httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTP] GET... code: %d\n", httpCode);

      // file found at server
      if(httpCode == HTTP_CODE_OK) {
          String payload = httpClient.getString();
          Serial.println(payload);
      }
  } else {
      Serial.printf("[HTTP] GET... failed, error: %s\n", httpClient.errorToString(httpCode).c_str());
  }
  Serial.print("[HTTP] end...");
  Serial.println(millis());
  httpClient.end();
}
